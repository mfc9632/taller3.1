# procesamiento de datos para el api
# primero se hace la importacion de las librerias
import mook


def buscador():
    # Se inicializa el diccionario
    indices = {}
    # Se definen las palabras claves para la busqueda
    llaves = ["violencia", "estafa", "alimentos", "salud", "pensión", "medicinas"]
    for tutela in mook.tutela:
        # .split divide las palabras del resumen de la tutela
        words = tutela["resumen"].split()
        # Itera sobre cada palabra en el resumen de la tutela
        for word in words:
            # Verifica si la palabra está en la lista de palabras clave.
            if word in llaves:
                # Verifica si la palabra ya está presente en el diccionario indices.
                if word in indices:
                    # Si la palabra ya está en indices, agrega la tutela actual a la lista de tutelas asociadas con esa palabra clave
                    indices[word].append(tutela)
                else:
                    indices[word] = [tutela]
    return indices


diccionario = buscador()
print(diccionario.get("estafa", "no se encontro"))